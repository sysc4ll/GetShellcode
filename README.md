# GetShellcode

Petit outil Windows avec GUI écrit en C avec l'API Win32 pour obtenir les bytes / les octets formant un shellcode depuis un fichier binaire.

![screenshot](screenshot.png)
